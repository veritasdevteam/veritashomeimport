﻿Imports System.Net

Module Module1
    Private sCON As String = "server=198.143.98.122;database=veritashome;Min Pool Size=5;Max Pool Size=10000;Connect Timeout=60;User Id=sa;Password=NCC1701E"
    Private SQL As String
    Private sLine As String
    Private sIn As String()
    Private clC As New clsDBO
    Private clS As New clsDBO

    Sub Main()
        'ImportHomeContract()
        'ImportHomeSurcharge()
        RateNewContracts()
    End Sub

    Private Sub RateNewContracts()
        SQL = "select * from contract "
        SQL = SQL + "where credate >= '" & Today & "' "
        clC.OpenDB(SQL, sCON)
        If clC.RowCount > 0 Then
            For cnt = 0 To clC.RowCount - 1
                clC.GetRowNo(cnt)
                ClearRate()
                ProcessContractRate()
                ProcessContractSurchargeRate()

            Next
        End If
    End Sub

    Private Sub ProcessContractSurchargeRate()
        Dim lRateSheet As Long
        lRateSheet = GetRateSheet(clC.Fields("ratebookid"))
        SQL = "select * from contractsurcharge "
        SQL = SQL + "where contractid = " & clC.Fields("contractid")
        clS.OpenDB(SQL, sCON)
        If clS.RowCount > 0 Then
            For cnt = 0 To clS.RowCount - 1
                clS.GetRowNo(cnt)
                ContractSurchargeRating(lRateSheet)
            Next
        End If
    End Sub

    Private Sub ClearRate()
        Dim clCA As New clsDBO
        SQL = "delete contractamt "
        SQL = SQL + "where contractid = " & clC.Fields("contractid")
        clCA.RunSQL(SQL, sCON)
    End Sub

    Private Sub ProcessContractRate()
        Dim lRateSheet As Long
        lRateSheet = GetRateSheet(clC.Fields("ratebookid"))
        ContractRating(lRateSheet)

    End Sub

    Private Sub ContractSurchargeRating(xRateSheet As Long)
        Dim clR As New clsDBO
        Dim clCA As New clsDBO
        SQL = "select * from ratesheetsurcharge "
        SQL = SQL + "where ratesheetid = " & xRateSheet & " "
        SQL = SQL + "and plantypeid = " & clC.Fields("plantypeid") & " "
        SQL = SQL + "and deductid = " & clC.Fields("deductid") & " "
        SQL = SQL + "and termmonth = " & clC.Fields("termmonth") & " "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            For cnt = 0 To clR.RowCount - 1
                clR.GetRowNo(cnt)
                SQL = "select * from contractamt "
                SQL = SQL + "where contractid = " & clC.Fields("contractid") & " "
                SQL = SQL + "and ratetypeid = " & clR.Fields("ratetypeid") & " "
                clCA.OpenDB(SQL, sCON)
                If clCA.RowCount > 0 Then
                    clCA.GetRow()
                    clCA.Fields("amt") = CDbl(clCA.Fields("amt")) + CDbl(clR.Fields("amt"))
                Else
                    clCA.NewRow()
                    clCA.Fields("contractid") = clC.Fields("contractid")
                    clCA.Fields("ratetypeid") = clR.Fields("ratetypeid")
                    clCA.Fields("amt") = clR.Fields("amt")
                End If
                If clCA.RowCount = 0 Then
                    clCA.AddRow()
                End If
                clCA.SaveDB()
            Next
        End If
    End Sub

    Private Sub ContractRating(xRateSheet As Long)
        Dim clR As New clsDBO
        Dim clCA As New clsDBO
        SQL = "select * from ratesheetdetail "
        SQL = SQL + "where ratesheetid = " & xRateSheet & " "
        SQL = SQL + "and plantypeid = " & clC.Fields("plantypeid") & " "
        SQL = SQL + "and deductid = " & clC.Fields("deductid") & " "
        SQL = SQL + "and termmonth = " & clC.Fields("termmonth") & " "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            For cnt = 0 To clR.RowCount - 1
                clR.GetRowNo(cnt)
                SQL = "select * from contractamt "
                SQL = SQL + "where contractid = " & clC.Fields("contractid") & " "
                SQL = SQL + "and ratetypeid = " & clR.Fields("ratetypeid") & " "
                clCA.OpenDB(SQL, sCON)
                If clCA.RowCount > 0 Then
                    clCA.GetRow()
                Else
                    clCA.NewRow()
                    clCA.Fields("contractid") = clC.Fields("contractid")
                    clCA.Fields("ratetypeid") = clR.Fields("ratetypeid")
                End If
                clCA.Fields("amt") = clR.Fields("amt")
                If clCA.RowCount = 0 Then
                    clCA.AddRow()
                End If
                clCA.SaveDB()
            Next
        End If
    End Sub

    Private Function GetRateSheet(xRateBookID As Long) As Long
        Dim clR As New clsDBO
        GetRateSheet = 0
        SQL = "select * from ratebook "
        SQL = SQL + "where ratebookid = " & xRateBookID
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            GetRateSheet = clR.Fields("ratesheetid")
        End If
    End Function

    Private Sub ImportHomeSurcharge()
        Dim lContractID As Long
        Dim lSurchargeID As Long
        Dim clCS As New clsDBO
        FileOpen(1, "C:\Test\HomeSurcharge2021_03_16.txt", OpenMode.Input)
        sLine = LineInput(1)

        Do Until EOF(1)
            sLine = LineInput(1)
            sIn = sLine.Split(vbTab)
            lContractID = GetContractID(sIn(0))
            lSurchargeID = getsurchargeid(sIn(1))
            SQL = "select * from contractsurcharge "
            SQL = SQL + "where contractid = " & lContractID & " "
            SQL = SQL + "and surchargeid = " & lSurchargeID & " "
            clCS.OpenDB(SQL, sCON)
            If clCS.RowCount = 0 Then
                clCS.NewRow()
                clCS.Fields("contractid") = lContractID
                clCS.Fields("surchargeid") = lSurchargeID
                clCS.Fields("surchargeamt") = sIn(3)
                clCS.AddRow()
                clCS.SaveDB()
            End If
        Loop
    End Sub

    Private Function GetSurchargeID(xSurcharge As String) As Long
        GetSurchargeID = 0
        Dim clS As New clsDBO
        SQL = "select * from surcharge "
        SQL = SQL + "where surchargecode = '" & xSurcharge & "' "
        clS.OpenDB(SQL, sCON)
        If clS.RowCount > 0 Then
            clS.GetRow()
            GetSurchargeID = clS.Fields("surchargeid")
        End If
    End Function

    Private Function GetContractID(xContractNo As String) As Long
        GetContractID = 0
        Dim clC As New clsDBO
        SQL = "select * from contract "
        SQL = SQL + "where contractno = '" & xContractNo & "' "
        clC.OpenDB(SQL, sCON)
        If clC.RowCount > 0 Then
            clC.GetRow()
            GetContractID = clC.Fields("contractid")
        End If
    End Function

    Private Sub ImportHomeContract()
        FileOpen(1, "C:\Test\Home2021_03_16.txt", OpenMode.Input)
        sLine = LineInput(1)

        Do Until EOF(1)
            sLine = LineInput(1)
            sIn = sLine.Split(vbTab)
            ProcessContract()
        Loop
        FileClose(1)
    End Sub

    Private Sub ProcessContract()
        Dim sContractNo As String
        sContractNo = sIn(25)
        If Not VerifyContractNo(sContractNo) Then
            Exit Sub
        End If
        Dim clC As New clsDBO
        SQL = "select * from contract "
        SQL = SQL + "where contractno = '" & sContractNo & "' "
        clC.OpenDB(SQL, sCON)
        If clC.RowCount = 0 Then
            clC.NewRow()
            clC.Fields("contractno") = sContractNo
            clC.Fields("fname") = sIn(0)
            clC.Fields("lname") = sIn(1)
            clC.Fields("addr1") = sIn(2)
            clC.Fields("addr2") = sIn(3)
            clC.Fields("city") = sIn(4)
            clC.Fields("state") = sIn(5)
            clC.Fields("zip") = sIn(6)
            clC.Fields("phone") = sIn(7)
            clC.Fields("coveredaddr1") = sIn(8)
            clC.Fields("coveredaddr2") = sIn(9)
            clC.Fields("coveredcity") = sIn(10)
            clC.Fields("coveredstate") = sIn(11)
            clC.Fields("coveredzip") = sIn(12)
            clC.Fields("dealerid") = GetDealerID(sIn(13))
            clC.Fields("moxydealercost") = sIn(15)
            clC.Fields("customercost") = sIn(16)
            clC.Fields("retailrate") = sIn(17)
            clC.Fields("markup") = sIn(18)
            clC.Fields("surchargecost") = sIn(19)
            clC.Fields("saledate") = sIn(20)
            If sIn(21).ToLower = "appliance" Then
                clC.Fields("plantypeid") = 1
            End If
            If sIn(22) = 75 Then
                clC.Fields("deductid") = 1
            End If
            clC.Fields("termmonth") = sIn(23)
            clC.Fields("effdate") = DateAdd(DateInterval.Day, CInt(sIn(24)), CDate(clC.Fields("saledate")))
            clC.Fields("expdate") = DateAdd(DateInterval.Month, CInt(clC.Fields("termmonth")), CDate(clC.Fields("effdate")))
            If sIn(26).ToLower = "hpp" Then
                clC.Fields("programid") = 1
            End If
            clC.Fields("ratebookid") = CalcRateBook(clC.Fields("programid"))
            clC.Fields("lienholder") = sIn(28)
            Dim client As New WebClient
            client.DownloadFile(sIn(30), "c:\test\" & clC.Fields("contractno") & "-dec.pdf")
            client.DownloadFile(sIn(31), "c:\test\" & clC.Fields("contractno") & "-TC.pdf")
            clC.Fields("decpage") = "c:\test\" & clC.Fields("contractno") & "-dec.pdf"
            clC.Fields("tcpage") = "c:\test\" & clC.Fields("contractno") & "-TC.pdf"
            clC.Fields("creby") = 1
            clC.Fields("credate") = Today
            clC.AddRow()
            clC.SaveDB()
        End If
    End Sub

    Private Function CalcRateBook(xProgramID As Long) As Long
        CalcRateBook = 0
        Dim clRB As New clsDBO
        SQL = "select * from ratebook "
        SQL = SQL + "where programid = " & xProgramID & " "
        SQL = SQL + "and startdate <= '" & Today & "' "
        SQL = SQL + "and enddate >= '" & Today & "' "
        clRB.OpenDB(SQL, sCON)
        If clRB.RowCount > 0 Then
            clRB.GetRow()
            CalcRateBook = clRB.Fields("ratebookid")
        End If
    End Function

    Private Function GetDealerID(sDealerNo As String) As Long
        GetDealerID = 0
        Dim clD As New clsDBO
        SQL = "select * from dealer "
        SQL = SQL + "where dealerno = '" & sDealerNo & "' "
        clD.OpenDB(SQL, sCON)
        If clD.RowCount > 0 Then
            clD.GetRow()
            GetDealerID = clD.Fields("dealerid")
        End If
    End Function

    Private Function VerifyContractNo(xContractNo As String) As Boolean
        VerifyContractNo = False
        Dim clC As New clsDBO
        SQL = "select * from contract "
        SQL = SQL + "where contractno = '" & xContractNo & "' "
        clC.OpenDB(SQL, sCON)
        If clC.RowCount > 0 Then
            Exit Function
        End If
        VerifyContractNo = True
    End Function

End Module
